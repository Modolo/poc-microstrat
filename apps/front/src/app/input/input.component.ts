import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ademo-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent implements ControlValueAccessor {
  value1 = 1;
  value2 = 1;

  onChange!: (value: number) => void;

  registerOnChange(fn: (value: number) => void): void {
    this.onChange = fn;
  }

  decrement(index: number) {
    if (index === 1) {
      this.value1--;
    }
    this.onChange((this.value1 + this.value2) / 2);
  }

  add(index: number) {
    this.value1++;
    this.onChange(this.value1 * 10);
  }

  writeValue(obj: number): void {
    this.value1 = obj;
  }

  registerOnTouched(): void {
    // nothing to do
  }
}
