import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'ademo-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss'],
})
export class HelloComponent {
  readonly form = this.fb.group({
    email: '',
    password: '',
    inputValue: 2,
  });

  constructor(private fb: FormBuilder) {
    this.form.valueChanges.subscribe(console.log);
    setTimeout(() => this.form.patchValue({ inputValue: 5 }), 2000);
  }
}
